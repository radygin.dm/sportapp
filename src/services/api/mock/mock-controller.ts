import request from "../axios/axios-instance";

const mockController = {
  getNewsList() {
    const url = "/news";
    return request.get(url);
  },
};

export default mockController;
