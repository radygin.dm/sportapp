import axios from "axios";

// mock requests starts
import MockAdapter from "axios-mock-adapter";
const mock = new MockAdapter(axios, { delayResponse: 2000 });

// mock news
import news from "../mock/news-list.mock.json";
mock.onGet("/news").reply(200, news);

// mock request finished
const baseURL = "/";

export interface RequestParams {
  [key: string]: string | number | boolean;
}

export const axiosInsance = axios.create({
  baseURL,
  withCredentials: true,
});

export const getUrlSearchParams = (params: RequestParams) => {
  return (
    "?" +
    Object.keys(params)
      .map((key) => {
        return encodeURIComponent(key) + "=" + encodeURIComponent(params[key]);
      })
      .join("&")
  );
};

const request = {
  get<T>(url: string, params?: object): Promise<T> {
    return axiosInsance.get(url, { params }).then(
      (response) => response.data,
      (error) => console.error(error)
    );
  },

  post<T>(url: string, body?: object, params?: RequestParams): Promise<T> {
    if (params) {
      url += getUrlSearchParams(params);
    }
    return axiosInsance.post(url, body).then(
      (response) => response.data,
      (error) => console.error(error)
    );
  },

  patch<T>(url: string, body?: object): Promise<T> {
    return axiosInsance.patch(url, body).then(
      (response) => response.data,
      (error) => console.error(error)
    );
  },

  put<T>(url: string, body?: object): Promise<T> {
    return axiosInsance.put(url, body).then(
      (response) => response.data,
      (error) => console.error(error)
    );
  },

  delete<T>(url: string): Promise<T> {
    return axiosInsance.delete(url).then(
      (response) => response.data,
      (error) => console.error(error)
    );
  },
};

export default request;
