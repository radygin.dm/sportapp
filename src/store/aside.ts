import { defineStore } from "pinia";

export const useAside = defineStore("aside", {
    state: () => ({
        isOpen: true,
        isSlide: false,
    }),
});
