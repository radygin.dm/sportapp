import { defineStore } from "pinia";
import { IUser, User } from "../models/user.model";

const useUserStore = defineStore("user", {
  state: () => ({
    user: new User(),
  }),
  getters: {
    getUser: (state) => state.user,
  },
  actions: {
    setUser(user: IUser) {
      this.user = user;
    },
  },
});

export default useUserStore;
