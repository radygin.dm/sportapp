import { defineStore } from "pinia";

export const useUser = defineStore("main", {
    state: () => ({
        counter: 0,
        name: "Roberto",
        isAdmin: true,
    }),
});
