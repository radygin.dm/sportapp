import { defineStore } from "pinia";

// useStore could be anything like useUser, useCart
// the first argument is a unique id of the store across your application
type ListStore = {
    list: {
        name: string;
        count: number;
    }[];
};

export const useList = defineStore("list", {
    state: () =>
        ({
            list: [
                {
                    count: 4,
                },
                {
                    name: 7685,
                    count: 4,
                },
                {
                    name: "list",
                    count: 4,
                },
            ],
        } as ListStore),
});
