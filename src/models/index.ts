export interface SimplifyModel<T = number, K = string> {
  id: T;
  name: K;
}

export interface Paginations {
  page: number;
  count: number;
}
