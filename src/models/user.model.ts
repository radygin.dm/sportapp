import { SimplifyModel } from ".";

export interface IUser {
  photo?: string;
  name: string;
  email: string;
  phone: string;
  birday: Date | string;
  growth?: number;
  curLevel?: SimplifyModel | null;
  level?: Array<SimplifyModel> | null;
  curRole?: SimplifyModel | null;
  role?: Array<SimplifyModel> | null;
}

export class User {
  photo?: string;
  name: string;
  email: string;
  phone: string;
  readonly birday: Date | string;
  growth?: number;
  curLevel?: SimplifyModel | null;
  level?: Array<SimplifyModel> | null;
  curRole?: SimplifyModel | null;
  role?: Array<SimplifyModel> | null;

  get isValidName(): boolean {
    return !!this.name;
  }

  get isValidEmail(): boolean {
    return !!this.email;
  }

  get isValidPhone(): boolean {
    return !!this.phone;
  }

  get isValidBirday(): boolean {
    return !!this.phone;
  }

  constructor(user?: IUser) {
    this.photo = user?.photo || "";
    this.name = user?.name || "";
    this.email = user?.email || "";
    this.phone = user?.phone || "";
    this.birday = user?.birday || "";
    this.growth = user?.growth || 0;
    this.curLevel = user?.curLevel || null;
    this.level = user?.level || null;
    this.curRole = user?.curRole || null;
    this.role = user?.role || null;
  }
}
