import { Paginations } from "..";

export interface INewItem {
  img: string;
  id: number;
  title: string;
  date: string;
  preview_text?: string;
  detail_text?: string;
  author?: string;
}

export interface INewsList {
  list: Array<INewItem>;
  paginations: Paginations;
}
