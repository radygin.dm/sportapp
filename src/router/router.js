import { createWebHistory, createRouter } from "vue-router";
import Home from "@/views/home.vue";
import About from "@/views/about.vue";
import NewsPage from "@/views/news/news.vue";
import NewsDetail from "@/views/news/news-detail.vue";
import Rating from "@/views/rating.vue";
import TrainingList from "@/views/training-list.vue";
import TurnamentList from "@/views/turnament-list.vue";
import Auth from "@/views/auth.vue";
import Registration from "@/views/registration.vue";
import TurnamentDetail from "@/views/turnament-detail.vue";
import UiKit from "@/views/ui-kit.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    component: About,
  },
  {
    path: "/news",
    name: "NewsPage",
    component: NewsPage,
  },
  {
    path: "/news/:id",
    name: "NewsDetail",
    component: NewsDetail,
  },
  {
    path: "/rating",
    name: "Rating",
    component: Rating,
  },
  {
    path: "/training",
    name: "TrainingList",
    component: TrainingList,
  },
  {
    path: "/turnament",
    name: "TurnamentList",
    component: TurnamentList,
  },
  {
    path: "/turnament/:code",
    name: "TurnamentDetail",
    component: TurnamentDetail,
  },
  {
    path: "/auth",
    name: "Auth",
    component: Auth,
  },
  {
    path: "/registration",
    name: "Register",
    component: Registration,
  },
  {
    path: "/ui-kit",
    name: "UiKit",
    component: UiKit,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
