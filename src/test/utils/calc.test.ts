import { describe, expect, test } from "vitest";
import calculation from "../../utils/calc";

test("calc", () => {
  expect(calculation(5)).toBe(10);
  expect(calculation(1)).toBe(6);
  expect(calculation(5)).not.toBe(11);
});
