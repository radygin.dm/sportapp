import { describe, expect, it } from "vitest";
import { User } from "../../models/user.model";
import useUserStore from "../../store/user";

describe("Класс пользователя", () => {
  const user = new User({
    name: "user",
    email: "user@example.com",
    phone: "8 999 999 99 99",
    birday: "20.10.1995",
  });

  it("Инстанс класса пользователя создается", () => {
    expect(user).toBeInstanceOf(User);
  });

  it("Пользователь создается корректно", () => {
    expect(user.name).toBe("user");
    expect(user.email).toBe("user@example.com");
    expect(user.phone).toBe("8 999 999 99 99");
    expect(user.birday).toBe("20.10.1995");
  });

  it("Поля пользователя редактируются", () => {
    user.name = "Pypkin";
    user.email = "Pypkin@example.com";
    user.phone = "8 999 999 00 00";

    expect(user.name).toBe("Pypkin");
    expect(user.email).toBe("Pypkin@example.com");
    expect(user.phone).toBe("8 999 999 00 00");
  });

  it("Валидация обязательных полей", () => {
    user.name = "";
    user.phone = "8 999 999 11 11";
    user.email = "";

    expect(user.isValidName).toBeFalsy();
    expect(user.isValidEmail).toBeFalsy();
    expect(user.isValidPhone).toBeTruthy();
    expect(user.isValidBirday).toBeTruthy();
  });
});
