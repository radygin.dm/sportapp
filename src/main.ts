import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";
import router from "@/router/router";
import InlineSvg from "vue-inline-svg";
import ElementPlus from "element-plus";
import "./assets/styles/base-tailwind.css";
import "element-plus/dist/index.css";
import "./index.css";

createApp(App)
  .use(router)
  .use(createPinia())
  .use(ElementPlus)
  .component("inline-svg", InlineSvg)
  .mount("#app");
