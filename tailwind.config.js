module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    colors: {
      current: "currentColor",

      primary: {
        100: "#FFFFFF",
        90: "#EADDFF",
        80: "#D0BCFF",
        70: "#B69DF8",
        60: "#9A82DB",
        50: "#7F67BE",
        40: "#6750A4",
        30: "#4F378B",
        20: "#381E72",
        10: "#21005D",
        0: "#000000",
      },

      secondary: {
        100: "#FFFFFF",
        90: "#E8DEF8",
        80: "#CCC2DC",
        70: "#B0A7C0",
        60: "#958DA5",
        50: "#7A7289",
        40: "#625B71",
        30: "#4A4458",
        20: "#332D41",
        10: "#1D192B",
        0: "#000000",
      },

      error: {
        100: "#FFFFFF",
        90: "#F9DEDC",
        80: "#F2B8B5",
        70: "#EC928E",
        60: "#E46962",
        50: "#DC362E",
        40: "#B3261E",
        30: "#8C1D18",
        20: "#601410",
        10: "#410E0B",
        0: "#000000",
      },

      ground: {
        100: "#141218",
        75: "#2B2930",
        50: "#CAC4D0",
        25: "#FEF7FF",
        0: "#000000",
      },

      transparent: "transparent",
      unset: "unset",
    },

    fontSize: {
      /* Основные шрифты */

      /* Заголовок раздела. Может быть только один на экране */
      h1: [
        "2.188rem",
        {
          fontWeight: 600,
          lineHeight: "2.188rem",
        },
      ],

      /* Заголовки элементов, всплывающих внутри раздела поверх основного контента */
      titleXl: [
        "1.688rem",
        {
          fontWeight: 600,
          lineHeight: "1.813rem",
        },
      ],

      /* Заголовки блоков в разделе */
      titleMd: [
        "1.3125rem",
        {
          fontWeight: 700,
          lineHeight: "1.313rem",
        },
      ],

      /* Заголовки в уведомлениях, целях */
      titleSm: [
        "1.3125rem",
        {
          lineHeight: "1.5rem",
          fontWeight: 600,
        },
      ],

      mapButtons: [
        "1.3125rem",
        {
          lineHeight: "1.5rem",
        },
      ],

      /* Наборный текст */
      input: [
        "1rem",
        {
          lineHeight: "1.25rem",
        },
      ],

      /* Кнопки */
      button: [
        "1rem",
        {
          lineHeight: "1.25rem",
          fontWeight: 500,
        },
      ],

      /* Заголовки внутри блоков */
      body: [
        "1rem",
        {
          lineHeight: "1.25rem",
          fontWeight: 600,
        },
      ],

      checkbox: [
        "1rem",
        {
          lineHeight: "0.875rem",
          fontWeight: 400,
        },
      ],

      /* Подписи и дескрипторы, заголовки в карточках клубов */
      small: [
        "0.875rem",
        {
          lineHeight: "1rem",
        },
      ],

      label: [
        "0.75rem",
        {
          fontWeight: 400,
          lineHeight: "1.125rem",
        },
      ],

      /* цифры, счетчики, тонкие подписи */
      count: [
        "0.875rem",
        {
          lineHeight: "1rem",
          fontWeight: 400,
        },
      ],

      "mobile-title": [
        "1.3125rem",
        {
          lineHeight: "1.5rem",
          fontWeight: 600,
        },
      ],
    },

    borderRadius: {
      none: 0,
      full: "100%",
      "2xl": "16px",
      xl: "12px",
      md: "8px",
      sm: "4px",
    },

    screens: {
      xs: "375px",
      s: "414px",
      sm: "768px",
      md: "1068px",
      lg: "1368px",
      xl: "1680px",
      xxl: "1920px",
    },
    extend: {
      boxShadow: {
        card_1:
          "0px 1px 3px 1px rgba(0, 0, 0, 0.15), 0px 1px 2px 0px rgba(0, 0, 0, 0.30)",
        card_2:
          "0px 2px 6px 2px rgba(0, 0, 0, 0.15), 0px 1px 2px 0px rgba(0, 0, 0, 0.30)",
        card_3:
          "0px 1px 3px 0px rgba(0, 0, 0, 0.30), 0px 4px 8px 3px rgba(0, 0, 0, 0.15)",
        card_4:
          "0px 2px 3px 0px rgba(0, 0, 0, 0.30), 0px 6px 10px 4px rgba(0, 0, 0, 0.15)",
        card_5:
          "0px 4px 4px 0px rgba(0, 0, 0, 0.30), 0px 8px 12px 6px rgba(0, 0, 0, 0.15)",
      },
    },
  },
  plugins: [],
};
