/* eslint-env node */
require("@rushstack/eslint-patch/modern-module-resolution");

module.exports = {
  root: true,
  extends: [
    "plugin:vue/vue3-essential",
    "eslint:recommended",
    "@vue/eslint-config-prettier",
    "prettier",
  ],
  env: {
    "vue/setup-compiler-macros": true,
  },
  rules: {
    /* Запрет debugger */
    "no-debugger": "error",
    /* Предупреждение console.log */
    "no-console": "warn",
    /* Предупреждает чтобы prop содержал определение типа */
    "vue/require-prop-types": "error",
    /* Предупреждает о неиспользуемых переменных */
    "vue/no-unused-properties": [
        "error",
        {
            "groups": [
                "props",
                "computed",
                "methods"
            ],
            "deepData": false,
            "ignorePublicMembers": false,
            "unreferencedOptions": []
        }
    ],
    /* Максимальная длинна строки */
    "max-len": ["warn", { "code": 140 }],
    /* Правило использования кебаб-кейса при объявлении атрибутов компонентов */
    "vue/attribute-hyphenation": ["error", "always", {
        "ignore": []
    }],
    /* Правило добавление отступов внутри интерполяции */
    "vue/mustache-interpolation-spacing": ["error", "always"],
    /* Запрет на пробелы между символами в атрибутах */
    "vue/no-spaces-around-equal-signs-in-attribute": ["error"],
    /* Максимальное количество атрибутов в строке */
    "vue/max-attributes-per-line": [
        "error",
        {
            "singleline": {
                "max": 2
            },
            "multiline": {
                "max": 1
            }
        }
    ],
    /* Настройка отступов */
    "vue/html-indent": [
        "warn",
        4,
        {
            "attribute": 1,
            "baseIndent": 1,
            "closeBracket": 0,
            "alignAttributesVertically": true,
            "ignores": []
        }
    ],
    /* Настройка отступов в блоке script */
    "vue/script-indent": ["error", 4, { "baseIndent": 0 }],
    /* Настройка регистра стиля именования компонентов в шаблоне */
    "vue/component-name-in-template-casing": ["error", "kebab-case", {
        "registeredComponentsOnly": true
    }],
    /* Настройка регистра именования компонентов в script */
    "vue/component-definition-name-casing": ["error", "PascalCase"],
    /* Сокращение для true prop */
    "vue/prefer-true-attribute-shorthand": ["error", "always"],
    /* Настройка отступов между блоками script template style */
    "vue/padding-line-between-blocks": ["error", "always"],
    /* Настройка пустых строк */
    "no-multiple-empty-lines": ["error", { "max": 1, "maxEOF": 0 }],
    /* Предупреждение для инлайн стилей */
    "vue/no-static-inline-styles": ["warn"],
    /* Предупреждения для переезда на vue3 */
    "vue/no-deprecated-data-object-declaration": "error",
    "vue/no-deprecated-filter": "error",
    "vue/no-deprecated-functional-template": "error",
    "vue/no-deprecated-events-api": "warn",
    "vue/no-deprecated-props-default-this": "warn",
    "vue/no-deprecated-scope-attribute": "warn",
    "vue/no-deprecated-slot-attribute": "warn",
    "vue/no-deprecated-slot-scope-attribute": "warn",
    "vue/no-deprecated-v-on-number-modifiers": "warn",
    "vue/no-deprecated-vue-config-keycodes": "warn"
}
};
